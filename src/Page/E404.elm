module Page.E404 exposing (view)

import Element exposing (Element, centerX, centerY, el, text)


view : String -> Element msg
view error =
    el [ centerX, centerY ] (text error)
