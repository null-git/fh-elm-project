module Page.Utils exposing
    ( compareTime
    , compareTime2
    , date
    , dateTime
    , dynamicTime
    , eitherOr
    , ifAppend
    , listIcon
    , maybeAppend
    , removeLast
    , stat
    , tag
    , time
    , title
    )

import DateFormat as DF
import Element
    exposing
        ( Attribute
        , Element
        , el
        , html
        , htmlAttribute
        , padding
        , row
        , text
        )
import Element.Background as Background
import Element.Font as Font
import FeatherIcons exposing (Icon)
import Html.Attributes as Attributes
import Style
import Time



-- Monad helpers


maybeAppend : (a -> b) -> Maybe a -> List b -> List b
maybeAppend func maybe list =
    case maybe of
        Just a ->
            func a :: list

        Nothing ->
            list


removeLast : List a -> List a
removeLast list =
    case list of
        [] ->
            []

        _ :: [] ->
            []

        x :: xs ->
            x :: removeLast xs


eitherOr : Bool -> a -> a -> a
eitherOr cond a b =
    if cond then
        a

    else
        b


ifAppend : Bool -> a -> List a -> List a
ifAppend cond elem list =
    if cond then
        elem :: list

    else
        list



-- Element containers


title : String -> Attribute msg
title text =
    htmlAttribute (Attributes.title text)


stat : String -> Int -> Element msg
stat name count =
    tag name (String.fromInt count)


tag : String -> String -> Element msg
tag name value =
    el [ Background.color Style.blue, Font.color Style.white, Style.fontSmall ] <|
        row []
            [ el
                [ padding Style.halfSpace, Font.heavy ]
                (text value)
            , el
                [ padding Style.halfSpace, Background.color Style.lightBlue ]
                (text name)
            ]


listIcon : Icon -> Element msg
listIcon icon =
    FeatherIcons.withSize 16 icon
        |> FeatherIcons.toHtml [ Attributes.style "margin-top" "-4px" ]
        |> html


date : Time.Posix -> String
date posix =
    DF.format
        [ DF.yearNumber
        , DF.text "-"
        , DF.monthFixed
        , DF.text "-"
        , DF.dayOfMonthFixed
        ]
        Time.utc
        posix


time : Time.Posix -> String
time posix =
    DF.format
        [ DF.hourMilitaryFixed
        , DF.text ":"
        , DF.minuteFixed
        , DF.text ":"
        , DF.secondFixed
        ]
        Time.utc
        posix


dateTime : Time.Posix -> String
dateTime posix =
    date posix ++ " " ++ time posix


dynamicTime : Time.Posix -> Time.Posix -> String
dynamicTime earlierPosix nowPosix =
    let
        earlierMinutes =
            Time.posixToMillis earlierPosix // 1000 // 60

        nowMinutes =
            Time.posixToMillis nowPosix // 1000 // 60

        differenceMinutes =
            nowMinutes - earlierMinutes
    in
    if differenceMinutes < 2 then
        "Just now"

    else if differenceMinutes < 60 then
        String.fromInt differenceMinutes ++ " minutes ago"

    else if differenceMinutes < 1440 then
        time earlierPosix

    else if differenceMinutes < 525600 then
        date earlierPosix

    else
        String.fromInt (Time.toYear Time.utc earlierPosix)


compareTime : (record -> Time.Posix) -> record -> record -> Order
compareTime accessor a b =
    compare (Time.posixToMillis (accessor a)) (Time.posixToMillis (accessor b))


compareTime2 : (recordA -> recordB) -> (recordB -> Time.Posix) -> recordA -> recordA -> Order
compareTime2 accessSub accessTime a b =
    compare (Time.posixToMillis (accessSub a |> accessTime)) (Time.posixToMillis (accessSub b |> accessTime))
