module Discussion exposing
    ( Discussion(..)
    , NID(..)
    , Note
    , Thread(..)
    , decoder
    , nidStr
    , noteFromIssue
    )

import Iso8601
import Issue exposing (Issue, iidStr)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline as Pipeline
import Time
import User exposing (User)


{-| A issue discussion is a list of discussion threads which are just lists of notes
-}
type Discussion
    = Discussion (List Thread)


type Thread
    = Thread (List Note)


type NID
    = NID String


type alias Note =
    { id : NID
    , body : String
    , author : User
    , createdAt : Time.Posix
    , updatedAt : Time.Posix
    }


nidStr : NID -> String
nidStr (NID nid_) =
    nid_


noteFromIssue : Issue -> Note
noteFromIssue issue =
    { id = NID (iidStr issue.id)
    , body = issue.description
    , author = issue.author
    , createdAt = issue.createdAt
    , updatedAt = issue.updatedAt
    }


decodeNID : Decoder NID
decodeNID =
    Decode.map (String.fromInt >> NID) Decode.int


decodeNote : Decoder Note
decodeNote =
    Decode.succeed Note
        |> Pipeline.required "id" decodeNID
        |> Pipeline.required "body" Decode.string
        |> Pipeline.required "author" User.decoder
        |> Pipeline.required "created_at" Iso8601.decoder
        |> Pipeline.required "updated_at" Iso8601.decoder


decodeThread : Decoder Thread
decodeThread =
    Decode.succeed Thread
        |> Pipeline.required "notes" (Decode.list decodeNote)



-- Decode.map Thread (Decode.list decodeNote)


decoder : Decoder Discussion
decoder =
    Decode.map Discussion (Decode.list decodeThread)
