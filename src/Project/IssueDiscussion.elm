module Project.IssueDiscussion exposing (fetch, view)

import Api
import Discussion exposing (Discussion, Note, Thread)
import Element
    exposing
        ( Element
        , alignLeft
        , alignRight
        , column
        , el
        , fill
        , none
        , paragraph
        , row
        , spacing
        , text
        , width
        )
import Element.Font as Font
import Issue exposing (IID, Issue)
import Page.Status as Status exposing (Status)
import Page.Utils as Utils
import Project exposing (PID)
import Project.Messages exposing (Msg(..), Page(..))
import Style
import Time
import User


fetch : PID -> IID -> Cmd Msg
fetch pid iid =
    Api.get
        (Api.discussionListEndpoint pid iid)
        GotIssueDiscussion
        Discussion.decoder


printNote : Time.Posix -> Maybe String -> Note -> Element Msg
printNote _ maybeTitle note =
    let
        titleElement : Element msg
        titleElement =
            case maybeTitle of
                Just title ->
                    paragraph [ Style.fontHeader ] [ text title ]

                Nothing ->
                    none
    in
    column [ spacing Style.defaultSpace, width fill ]
        [ row [ width fill, Style.fontSmall, Font.bold ]
            [ el [ alignLeft ] (text (User.toString note.author))
            , el [ alignRight ] (text (Utils.dateTime note.createdAt))
            ]
        , titleElement
        , paragraph [] [ text note.body ]
        ]


printThread : Time.Posix -> Maybe String -> Thread -> Element Msg
printThread now maybeTitle (Discussion.Thread thread) =
    let
        buildThread : List Note -> List (Element Msg)
        buildThread notes =
            case notes of
                [] ->
                    []

                x :: xs ->
                    printNote now maybeTitle x :: buildThread xs
    in
    column (spacing Style.doubleSpace :: Style.projectContent) (buildThread thread)


view : Time.Posix -> Issue -> Status Discussion -> Element Msg
view now issue statusDiscussion =
    let
        printDiscussion : Discussion -> Element Msg
        printDiscussion (Discussion.Discussion discussion) =
            column [ width fill, spacing Style.defaultSpace ]
                (printThread now (Just issue.title) (Discussion.Thread [ Discussion.noteFromIssue issue ])
                    :: List.map (printThread now Nothing) discussion
                )
    in
    Status.view "discussion" statusDiscussion printDiscussion Nothing
