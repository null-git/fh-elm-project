module Project.Messages exposing (Msg(..), Page(..))

import Branch exposing (Branch, BranchName)
import Discussion exposing (Discussion)
import Http
import Issue exposing (Issue)
import Project exposing (Inode, Project)
import Time


type Page
    = BranchList
    | ProjectDetail (Maybe BranchName)
    | ProjectList
    | IssueList
    | IssueDiscussion Issue
    | IssueCreate


type Msg
    = GotProjectDetail (Result Http.Error Project)
    | GotProjectFiles (Result Http.Error (List Inode))
    | GotClickedFile Inode
    | GotBranchList (Result Http.Error (List Branch))
    | GotIssueList (Result Http.Error (List Issue))
    | GotNewIssue (Result Http.Error Issue)
    | GotIssueDiscussion (Result Http.Error Discussion)
    | GotNewIssueTitle String
    | GotNewIssueText String
    | GotNewIssueButton
    | Load Page
    | UpdateTime Time.Posix
