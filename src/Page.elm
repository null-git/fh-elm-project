module Page exposing (fullscreenView, projectView)

import Browser exposing (Document)
import Element
    exposing
        ( Element
        , centerX
        , centerY
        , column
        , el
        , fill
        , fillPortion
        , height
        , layout
        , maximum
        , none
        , paddingXY
        , row
        , width
        )
import Element.Background as Background
import Project.Menu as Menu
import Project.Messages exposing (Msg)
import Style


projectView : String -> Element Msg -> Document Msg
projectView title page =
    { title = title
    , body =
        [ layout [ Background.color Style.background ] <|
            row [ height fill, width fill ]
                [ Menu.view
                , column
                    [ height fill
                    , width <| fillPortion 5
                    , paddingXY 0 (Style.doubleSpace * 4)
                    ]
                    [ page ]
                ]
        ]
    }


fullscreenView : String -> Element msg -> Document msg
fullscreenView title page =
    { title = title
    , body =
        [ layout [ Background.color Style.background ] <|
            el [ height fill, width fill, centerX, centerY ]
                (column
                    [ height fill, width fill ]
                    [ el [ height (fill |> maximum 40) ] none, page ]
                )
        ]
    }
