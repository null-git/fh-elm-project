module Style exposing
    ( background
    , blue
    , defaultSpace
    , doubleSpace
    , fontHeader
    , fontMenu
    , fontSmall
    , halfSpace
    , lightBlue
    , projectContent
    , white
    )

import Element
    exposing
        ( Attribute
        , Color
        , centerX
        , fill
        , maximum
        , padding
        , rgb255
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font



-- Colors


white : Color
white =
    rgb255 255 255 255


blue : Color
blue =
    rgb255 92 99 128


lightBlue : Color
lightBlue =
    rgb255 132 139 168


shadowColor : Color
shadowColor =
    rgb255 162 169 198


background : Color
background =
    rgb255 166 188 214



-- Spacing


defaultSpace : Int
defaultSpace =
    12


halfSpace : Int
halfSpace =
    defaultSpace // 2


doubleSpace : Int
doubleSpace =
    defaultSpace * 2



-- Fonts


fontHeader : Attribute msg
fontHeader =
    Font.size 32


fontMenu : Attribute msg
fontMenu =
    Font.size 24


fontSmall : Attribute msg
fontSmall =
    Font.size 14



-- Layout


projectContent : List (Attribute msg)
projectContent =
    [ centerX
    , width (fill |> maximum 800)
    , padding doubleSpace
    , Border.glow shadowColor 2.0
    , Background.color white
    ]
