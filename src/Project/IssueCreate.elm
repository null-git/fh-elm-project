module Project.IssueCreate exposing (post, view)

import Api
import Element
    exposing
        ( Element
        , alignRight
        , column
        , fill
        , height
        , mouseOver
        , padding
        , px
        , spacing
        , text
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Http
import Issue exposing (Issue)
import Json.Encode as Encode
import Project exposing (PID, pidStr)
import Project.Messages exposing (Msg(..), Page(..))
import Style


post : PID -> String -> String -> Cmd Msg
post pid title body =
    Api.post
        (Api.issueCreateEndpoint pid)
        (Http.jsonBody
            (Encode.object
                [ ( "id", Encode.string (pidStr pid) )
                , ( "title", Encode.string title )
                , ( "description", Encode.string body )
                ]
            )
        )
        GotNewIssue
        Issue.decoder


view : String -> String -> Element Msg
view title body =
    column (spacing Style.defaultSpace :: Style.projectContent)
        [ Input.text [ width fill, Border.rounded 0 ]
            { label = Input.labelHidden "title"
            , text = title
            , placeholder = Just (Input.placeholder [] (text "Title"))
            , onChange = GotNewIssueTitle
            }
        , Input.multiline [ width fill, height (px 120), Border.rounded 0 ]
            { label = Input.labelHidden "text"
            , text = body
            , placeholder = Just (Input.placeholder [] (text "Issue description"))
            , onChange = GotNewIssueText
            , spellcheck = True
            }
        , Input.button
            [ alignRight
            , Background.color Style.blue
            , padding Style.defaultSpace
            , Font.color Style.white
            , mouseOver [ Background.color Style.lightBlue ]
            ]
            { label = text "Submit"
            , onPress = Just GotNewIssueButton
            }
        ]
