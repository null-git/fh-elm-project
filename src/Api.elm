module Api exposing
    ( Endpoint
    , branchListEndpoint
    , discussionListEndpoint
    , get
    , issueCreateEndpoint
    , issueListEndpoint
    , post
    , projectDetailEndpoint
    , projectFilesEndpoint
    , projectListEndpoint
    )

import AuthKey
import Branch exposing (BranchName)
import Http
import Issue exposing (IID, iidStr)
import Json.Decode exposing (Decoder)
import Project exposing (PID, pidStr)
import Url.Builder exposing (QueryParameter)


apiUrl : String
apiUrl =
    "https://gitlab.com/api/v4"



-- Endpoints


type Endpoint
    = Endpoint String


expose : Endpoint -> String
expose (Endpoint path) =
    path


buildUrl : List String -> List QueryParameter -> Endpoint
buildUrl paths params =
    Url.Builder.crossOrigin
        apiUrl
        paths
        params
        |> Endpoint


projectListEndpoint : Endpoint
projectListEndpoint =
    buildUrl
        [ "projects" ]
        [ Url.Builder.string "membership" "true"
        , Url.Builder.string "statistics" "true"
        ]


projectDetailEndpoint : PID -> Endpoint
projectDetailEndpoint pid =
    buildUrl
        [ "projects", pidStr pid ]
        [ Url.Builder.string "statistics" "true" ]


projectFilesEndpoint : PID -> BranchName -> List String -> Endpoint
projectFilesEndpoint pid branchName path =
    buildUrl
        [ "projects", pidStr pid, "repository", "tree" ]
        [ Url.Builder.string "ref" (Branch.toString branchName)
        , Url.Builder.string "path" (String.join "/" path)
        ]


branchListEndpoint : PID -> Endpoint
branchListEndpoint pid =
    buildUrl
        [ "projects", pidStr pid, "repository", "branches" ]
        []


issueListEndpoint : PID -> Endpoint
issueListEndpoint pid =
    buildUrl
        [ "projects", pidStr pid, "issues" ]
        []


issueCreateEndpoint : PID -> Endpoint
issueCreateEndpoint pid =
    issueListEndpoint pid


discussionListEndpoint : PID -> IID -> Endpoint
discussionListEndpoint pid iid =
    buildUrl
        [ "projects", pidStr pid, "issues", iidStr iid, "discussions" ]
        []


{-| Http.request, except it takes an Endpoint instead of a Url.
-}
request :
    { method : String
    , headers : List Http.Header
    , endpoint : Endpoint
    , body : Http.Body
    , expect : Http.Expect msg
    , timeout : Maybe Float
    , tracker : Maybe String
    }
    -> Cmd msg
request config =
    Http.request
        { method = config.method
        , headers = AuthKey.asHeader :: config.headers
        , url = expose config.endpoint
        , body = config.body
        , expect = config.expect
        , timeout = config.timeout
        , tracker = config.tracker
        }


get : Endpoint -> (Result Http.Error a -> msg) -> Decoder a -> Cmd msg
get endpoint msg decoder =
    request
        { method = "GET"
        , headers = []
        , endpoint = endpoint
        , body = Http.emptyBody
        , expect = Http.expectJson msg decoder
        , timeout = Nothing
        , tracker = Nothing
        }


post : Endpoint -> Http.Body -> (Result Http.Error a -> msg) -> Decoder a -> Cmd msg
post endpoint body msg decoder =
    request
        { method = "POST"
        , headers = []
        , endpoint = endpoint
        , body = body
        , expect = Http.expectJson msg decoder
        , timeout = Nothing
        , tracker = Nothing
        }
