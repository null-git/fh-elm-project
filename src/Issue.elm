module Issue exposing
    ( IID(..)
    , Issue
    , State(..)
    , decoder
    , iidStr
    , isClosed
    , isOpen
    , toString
    )

import Iso8601
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline as Pipeline
import Time
import User exposing (User)


type State
    = Open
    | Closed


type IID
    = IID String


type alias Issue =
    { id : IID
    , title : String
    , description : String
    , state : State
    , createdAt : Time.Posix
    , updatedAt : Time.Posix
    , dueDate : Maybe Time.Posix
    , closedAt : Maybe Time.Posix
    , closedBy : Maybe User
    , labels : List String
    , author : User
    , assignee : Maybe User
    , upVotes : Int
    , downVotes : Int
    }


toString : Issue -> String
toString issue =
    if String.length issue.title > 46 then
        String.left 45 issue.title ++ "…"

    else
        issue.title


iidStr : IID -> String
iidStr (IID iid_) =
    iid_


isOpen : Issue -> Bool
isOpen issue =
    case issue.state of
        Open ->
            True

        Closed ->
            False


isClosed : Issue -> Bool
isClosed issue =
    not (isOpen issue)


stateFromString : String -> State
stateFromString state =
    if state == "opened" then
        Open

    else
        Closed


stateDecoder : Decoder State
stateDecoder =
    Decode.map stateFromString Decode.string


decodeIID : Decoder IID
decodeIID =
    Decode.map (String.fromInt >> IID) Decode.int


decoder : Decoder Issue
decoder =
    Decode.succeed Issue
        |> Pipeline.required "iid" decodeIID
        |> Pipeline.required "title" Decode.string
        |> Pipeline.optional "description" Decode.string ""
        |> Pipeline.required "state" stateDecoder
        |> Pipeline.required "created_at" Iso8601.decoder
        |> Pipeline.required "updated_at" Iso8601.decoder
        |> Pipeline.required "due_date" (Decode.nullable Iso8601.decoder)
        |> Pipeline.required "closed_at" (Decode.nullable Iso8601.decoder)
        |> Pipeline.required "closed_by" (Decode.nullable User.decoder)
        |> Pipeline.required "labels" (Decode.list Decode.string)
        |> Pipeline.required "author" User.decoder
        |> Pipeline.required "assignee" (Decode.nullable User.decoder)
        |> Pipeline.required "upvotes" Decode.int
        |> Pipeline.required "downvotes" Decode.int
