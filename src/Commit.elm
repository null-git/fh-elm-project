module Commit exposing (CID(..), Commit, cidStr, decoder)

import Iso8601
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline as Pipeline
import Time


type CID
    = CID String


type alias Commit =
    { id : CID
    , createdAt : Time.Posix
    , title : String
    , message : String
    , authorName : String
    , authorEmail : String
    , authorDate : String
    }


cidStr : CID -> String
cidStr (CID cid_) =
    cid_


decodeId : Decoder CID
decodeId =
    Decode.map CID Decode.string


decoder : Decoder Commit
decoder =
    Decode.succeed Commit
        |> Pipeline.required "id" decodeId
        |> Pipeline.required "created_at" Iso8601.decoder
        |> Pipeline.required "title" Decode.string
        |> Pipeline.required "message" Decode.string
        |> Pipeline.required "author_name" Decode.string
        |> Pipeline.required "author_email" Decode.string
        |> Pipeline.required "authored_date" Decode.string
