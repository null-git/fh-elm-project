module Project.Menu exposing (view)

import Branch exposing (BranchName(..))
import Element
    exposing
        ( Element
        , column
        , el
        , fill
        , fillPortion
        , height
        , html
        , maximum
        , mouseOver
        , none
        , padding
        , pointer
        , px
        , row
        , spacing
        , text
        , width
        )
import Element.Background as Background
import Element.Events as Events
import Element.Font as Font
import FeatherIcons exposing (Icon)
import Html.Attributes as Attributes
import Project.Messages exposing (Msg(..), Page(..))
import Style


menuIcon : Maybe Icon -> Element msg
menuIcon maybeIcon =
    case maybeIcon of
        Just icon ->
            el []
                (icon
                    |> FeatherIcons.withSize 16
                    |> FeatherIcons.toHtml [ Attributes.style "margin-top" "-4px" ]
                    |> html
                )

        Nothing ->
            el [ width (px 16) ] none


menuLink : ( String, Maybe Icon, Msg ) -> Element Msg
menuLink ( label, maybeIcon, msg ) =
    row
        [ width fill
        , padding Style.defaultSpace
        , Font.heavy
        , mouseOver [ Background.color Style.lightBlue ]
        , Events.onClick msg
        , pointer
        , spacing Style.defaultSpace
        ]
        [ menuIcon maybeIcon, text label ]


view : Element Msg
view =
    let
        menuItems =
            [ ( "Projects", Just FeatherIcons.chevronLeft, Load ProjectList )
            , ( "Details", Just FeatherIcons.home, Load <| ProjectDetail Nothing )
            , ( "Branches", Just FeatherIcons.gitBranch, Load BranchList )
            , ( "Issues", Just FeatherIcons.alertCircle, Load IssueList )
            , ( "New issue", Just FeatherIcons.plusCircle, Load IssueCreate )
            ]
    in
    column
        [ height fill
        , width <| fillPortion 1
        , Background.color <| Style.blue
        , Font.color <| Style.white
        ]
        (el [ height (fill |> maximum 64) ] none :: List.map menuLink menuItems)
