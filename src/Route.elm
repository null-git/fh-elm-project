module Route exposing (Route(..), fromUrl, replaceUrl, toString)

import Browser.Navigation as Navigation
import Project exposing (PID, pidStr)
import Url exposing (Url)
import Url.Parser as Parser exposing ((</>), Parser, custom, oneOf, s)


type Route
    = Login
    | Logout
    | Profile
    | ProjectList
    | ProjectDetail PID


parser : Parser (Route -> a) a
parser =
    oneOf
        [ Parser.map ProjectList Parser.top
        , Parser.map Login (s "login")
        , Parser.map Logout (s "logout")
        , Parser.map Profile (s "profile")
        , Parser.map ProjectList (s "projects")
        , Parser.map ProjectDetail (s "projects" </> pidParser)
        ]


pidParser : Parser (PID -> a) a
pidParser =
    custom "PID" <|
        \segment ->
            if segment == "" then
                Nothing

            else
                Just (Project.PID segment)



-- Public helpers


replaceUrl : Navigation.Key -> Route -> Cmd msg
replaceUrl key route =
    Navigation.replaceUrl key (toString route)


fromUrl : Url -> Maybe Route
fromUrl url =
    -- The RealWorld spec treats the fragment like a path.
    -- This makes it *literally* the path, so we can proceed
    -- with parsing as if it had been a normal path all along.
    { url | path = Maybe.withDefault "projects" url.fragment, fragment = Nothing }
        |> Parser.parse parser



-- Path building


toString : Route -> String
toString page =
    "#/" ++ String.join "/" (routeToList page) ++ "/"


routeToList : Route -> List String
routeToList page =
    case page of
        Login ->
            [ "login" ]

        Logout ->
            [ "logout" ]

        Profile ->
            [ "profile" ]

        ProjectList ->
            [ "projects" ]

        ProjectDetail pid ->
            [ "projects", pidStr pid ]
