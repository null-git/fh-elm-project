module Page.ProjectList exposing (Model, Msg, init, update, view)

import Api
import Element
    exposing
        ( Attribute
        , Element
        , alignRight
        , centerX
        , column
        , el
        , fill
        , link
        , maximum
        , mouseOver
        , paddingXY
        , row
        , text
        , width
        )
import Element.Font as Font
import Http
import Json.Decode as Decode
import Page.Status as Status exposing (Status(..))
import Page.Utils as Utils
import Project exposing (Project)
import Route
import Session exposing (Session)
import Style
import Task
import Time



-- Models


type alias Model =
    { session : Session
    , projects : Status (List Project)
    , time : Time.Posix
    }


init : Session -> ( Model, Cmd Msg )
init session =
    ( { session = session
      , projects = Loading ".."
      , time = Time.millisToPosix 0
      }
    , Cmd.batch [ Task.perform UpdateTime Time.now, fetchList ]
    )



-- Update


type Msg
    = GotProjectList (Result Http.Error (List Project))
    | UpdateTime Time.Posix


fetchList : Cmd Msg
fetchList =
    Api.get
        Api.projectListEndpoint
        GotProjectList
        (Decode.list Project.decoder)


update : Msg -> Model -> ( Model, Cmd msg )
update msg model =
    case msg of
        UpdateTime time ->
            ( { model | time = time }, Cmd.none )

        GotProjectList result ->
            case result of
                Err error ->
                    ( { model | projects = Failed error }, Cmd.none )

                Ok projectList ->
                    ( { model | projects = Loaded projectList }, Cmd.none )



-- List View


projectListView : Time.Posix -> List Project -> Element msg
projectListView timeNow projects =
    let
        styledItem : List (Attribute msg) -> Element msg -> Element msg
        styledItem attributes element =
            el (paddingXY Style.halfSpace 0 :: attributes) element

        projectView : Project -> Element msg
        projectView project =
            row
                [ centerX
                , width (fill |> maximum 800)
                , paddingXY 0 Style.defaultSpace
                ]
                [ styledItem [] (text project.owner.name)
                , styledItem [] (text "/")
                , styledItem
                    [ Utils.title project.description ]
                    (link [ Font.bold, mouseOver [ Font.color Style.blue ] ]
                        { url = Route.toString (Route.ProjectDetail project.id)
                        , label = text project.name
                        }
                    )
                , styledItem
                    [ alignRight, Utils.title (Utils.dateTime project.lastActivity) ]
                    (Utils.tag (Utils.dynamicTime project.lastActivity timeNow) "Last activity")
                , styledItem [ alignRight ] (Utils.stat "Stars" project.starCount)
                , styledItem [ alignRight ] (Utils.stat "Issues" project.openIssuesCount)
                ]
    in
    column
        [ width fill ]
        (List.map projectView (List.reverse (List.sortWith (Utils.compareTime .lastActivity) projects)))


view : Model -> Element msg
view model =
    Status.view "projects" model.projects (projectListView model.time) Nothing
