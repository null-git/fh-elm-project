module Session exposing
    ( Session
    , isAuthenticated
    , navigationKey
    , userName
    , usernameToSession
    )

import Browser.Navigation as Navigation
import Page.Status exposing (Status(..))
import Project exposing (Project)
import Time
import User exposing (User)


type Authenticated
    = LoggedIn User
    | Guest


type alias Session =
    { user : Authenticated
    , key : Navigation.Key
    , time : Time.Posix
    , project : Status Project
    }


init : Authenticated -> Navigation.Key -> Session
init user key =
    { user = user, key = key, time = Time.millisToPosix 0, project = Loading "none" }


usernameToSession : Maybe User -> Navigation.Key -> Session
usernameToSession maybeUser key =
    case maybeUser of
        Just user ->
            init (LoggedIn user) key

        Nothing ->
            init Guest key


userName : Session -> String
userName session =
    case session.user of
        LoggedIn user ->
            User.toString user

        Guest ->
            "Guest"


isAuthenticated : Session -> Bool
isAuthenticated session =
    case session.user of
        LoggedIn _ ->
            True

        Guest ->
            False


navigationKey : Session -> Navigation.Key
navigationKey session =
    session.key
