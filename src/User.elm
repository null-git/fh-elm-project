module User exposing
    ( User
    , Username(..)
    , decoder
    , toString
    , usernameToString
    )

import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline as Pipeline


type Username
    = Username String


type alias User =
    { name : String
    , username : Username
    }


toString : User -> String
toString user =
    user.name


usernameToString : Username -> String
usernameToString (Username username) =
    username


usernameDecoder : Decoder Username
usernameDecoder =
    Decode.map Username Decode.string


decoder : Decoder User
decoder =
    Decode.succeed User
        |> Pipeline.required "name" Decode.string
        |> Pipeline.required "username" usernameDecoder
