module Page.ProjectDetail exposing (Model, init, update, view)

import Api
import Branch exposing (Branch, BranchName(..))
import Discussion exposing (Discussion)
import Element exposing (Element, centerX, column, el, fill, none, row, spacing, text, width)
import Element.Font as Font
import Issue exposing (Issue, iidStr)
import Page.Status as Status exposing (Status(..))
import Page.Utils as Utils
import Project exposing (Inode, PID, Project, pidStr)
import Project.BranchList as BranchList
import Project.FileList as FileList
import Project.IssueCreate as IssueCreate
import Project.IssueDiscussion as IssueDiscussion
import Project.IssueList as IssueList
import Project.Messages exposing (Msg(..), Page(..))
import Route
import Session exposing (Session)
import Style
import Task
import Time



-- Models


type alias Model =
    { session : Session
    , pid : PID
    , page : Page
    , branch : BranchName
    , project : Status Project
    , files : Status (List Inode)
    , branches : Status (List Branch)
    , issues : Status (List Issue)
    , newIssueTitle : String
    , newIssueBody : String
    , discussion : Status Discussion
    , path : List String
    , time : Time.Posix
    }


init : Session -> PID -> BranchName -> ( Model, Cmd Msg )
init session pid branchName =
    ( { session = session
      , page = ProjectDetail (Just branchName)
      , pid = pid
      , branch = branchName
      , project = Loading (pidStr pid)
      , files = Loading (pidStr pid)
      , branches = Loading (pidStr pid)
      , issues = Loading (pidStr pid)
      , newIssueTitle = ""
      , newIssueBody = ""
      , discussion = Loading (pidStr pid)
      , path = []
      , time = Time.millisToPosix 0
      }
    , Cmd.batch [ Task.perform UpdateTime Time.now, fetchInstance pid ]
    )



-- Update


fetchInstance : PID -> Cmd Msg
fetchInstance pid =
    Api.get
        (Api.projectDetailEndpoint pid)
        GotProjectDetail
        Project.decoder


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
        branchOrModel : Maybe BranchName -> BranchName
        branchOrModel maybeBranchName =
            case maybeBranchName of
                Just branchName ->
                    branchName

                Nothing ->
                    model.branch

        pidOrNone : Status Project -> PID
        pidOrNone statusProject =
            case statusProject of
                Loaded project ->
                    project.id

                _ ->
                    Project.PID "none"
    in
    case msg of
        UpdateTime time ->
            ( { model | time = time }, Cmd.none )

        GotProjectDetail result ->
            case result of
                Err error ->
                    ( { model | project = Failed error }, Cmd.none )

                Ok projectDetail ->
                    ( { model
                        | project = Loaded projectDetail
                        , branch = projectDetail.defaultBranch
                      }
                    , FileList.fetch model.pid model.branch model.path
                    )

        GotProjectFiles result ->
            case result of
                Err error ->
                    ( { model | files = Failed error }, Cmd.none )

                Ok projectFiles ->
                    ( { model | files = Loaded projectFiles }, Cmd.none )

        GotClickedFile inode ->
            case inode.fileType of
                Project.Directory ->
                    ( { model
                        | path = inode.path
                        , files = Loading ("/" ++ String.join "/" inode.path)
                      }
                    , FileList.fetch model.pid model.branch inode.path
                    )

                _ ->
                    ( model, Cmd.none )

        GotBranchList result ->
            case result of
                Err error ->
                    ( { model | branches = Failed error }, Cmd.none )

                Ok projectBranches ->
                    ( { model | branches = Loaded projectBranches }, Cmd.none )

        GotIssueList result ->
            case result of
                Err error ->
                    ( { model | issues = Failed error }, Cmd.none )

                Ok projectIssues ->
                    ( { model | issues = Loaded projectIssues }, Cmd.none )

        GotIssueDiscussion result ->
            case result of
                Err error ->
                    ( { model | discussion = Failed error }, Cmd.none )

                Ok issueDiscussion ->
                    ( { model | discussion = Loaded issueDiscussion }, Cmd.none )

        GotNewIssueTitle issueTitle ->
            ( { model | newIssueTitle = issueTitle }, Cmd.none )

        GotNewIssueText issueBody ->
            ( { model | newIssueBody = issueBody }, Cmd.none )

        GotNewIssueButton ->
            if model.newIssueTitle == "" then
                ( model, Cmd.none )

            else
                ( { model
                    | page = IssueList
                    , issues = Loading (pidStr model.pid)
                    , newIssueTitle = ""
                    , newIssueBody = ""
                  }
                , IssueCreate.post model.pid model.newIssueTitle model.newIssueBody
                )

        GotNewIssue result ->
            case result of
                Err error ->
                    ( { model | issues = Failed error }, Cmd.none )

                Ok _ ->
                    (  model
                      
                    , IssueList.fetch model.pid
                    )

        Load page ->
            case page of
                ProjectList ->
                    ( model
                    , Route.replaceUrl
                        (Session.navigationKey model.session)
                        Route.ProjectList
                    )

                ProjectDetail branchName ->
                    ( { model
                        | page = ProjectDetail branchName
                        , branch = branchOrModel branchName
                        , path = []
                        , files = Loading (pidStr model.pid)
                      }
                    , FileList.fetch model.pid (branchOrModel branchName) []
                    )

                BranchList ->
                    ( { model
                        | page = BranchList
                        , branches = Loading (pidStr model.pid)
                      }
                    , BranchList.fetch model.pid
                    )

                IssueList ->
                    ( { model
                        | page = IssueList
                        , issues = Loading (pidStr model.pid)
                      }
                    , IssueList.fetch model.pid
                    )

                IssueDiscussion issue ->
                    ( { model
                        | page = IssueDiscussion issue
                        , discussion = Loading (iidStr issue.id)
                      }
                    , IssueDiscussion.fetch (pidOrNone model.project) issue.id
                    )

                IssueCreate ->
                    ( { model
                        | page = IssueCreate
                        , newIssueTitle = ""
                        , newIssueBody = ""
                      }
                    , Cmd.none
                    )



-- Detail View


projectHeaderView : Project -> Element Msg
projectHeaderView project =
    column [ width fill ]
        [ el [ centerX ] (text project.owner.name)
        , el [ centerX, Style.fontHeader, Font.heavy ] (text project.name)
        ]


projectTagView : Model -> Project -> Element Msg
projectTagView model project =
    row [ centerX, spacing Style.defaultSpace ]
        (Utils.maybeAppend (Utils.tag "Branch")
            (Branch.toMaybe model.branch)
            [ Utils.stat "Commits" project.commitCount
            , Utils.stat "Issues" project.openIssuesCount
            , Utils.stat "Forks" project.forksCount
            , Utils.stat "Stars" project.starCount
            ]
        )


pageView : Model -> Project -> Element Msg
pageView model project =
    case model.page of
        ProjectDetail _ ->
            column [ width fill, spacing Style.doubleSpace ]
                [ projectHeaderView project
                , projectTagView model project
                , FileList.view model.files model.path
                ]

        BranchList ->
            column [ width fill, spacing Style.doubleSpace ]
                [ projectHeaderView project
                , BranchList.view model.time model.branches
                ]

        IssueList ->
            column [ width fill, spacing Style.doubleSpace ]
                [ projectHeaderView project
                , IssueList.view model.issues
                ]

        IssueDiscussion issue ->
            column [ width fill, spacing Style.doubleSpace ]
                [ projectHeaderView project
                , IssueDiscussion.view model.time issue model.discussion
                ]

        IssueCreate ->
            column [ width fill, spacing Style.doubleSpace ]
                [ projectHeaderView project
                , IssueCreate.view model.newIssueTitle model.newIssueBody
                ]

        _ ->
            none


view : Model -> Element Msg
view model =
    Status.view "project" model.project (pageView model) Nothing
