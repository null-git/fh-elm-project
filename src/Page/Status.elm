module Page.Status exposing (Status(..), failedView, infoView, loadingView, view)

import Element exposing (Element, centerX, el, padding, text)
import Http
import Style


type Status a
    = Loading String
    | Loaded a
    | Failed Http.Error


errorToString : Http.Error -> String
errorToString error =
    case error of
        Http.BadUrl url ->
            "Malformed Url: " ++ url

        Http.Timeout ->
            "Timeout"

        Http.NetworkError ->
            "Network Error"

        Http.BadStatus status ->
            "Bad Status: " ++ String.fromInt status

        Http.BadBody message ->
            "Malformed Body: " ++ message


infoView : String -> Element msg
infoView info =
    el Style.projectContent (el [ centerX, padding Style.doubleSpace ] (text info))


loadingView : String -> String -> Element msg
loadingView instance info =
    infoView ("Loading " ++ instance ++ " (" ++ info ++ ")")


failedView : String -> Http.Error -> Element msg
failedView instance error =
    infoView ("Error while loading " ++ instance ++ " (" ++ errorToString error ++ ")")


view : String -> Status a -> (a -> Element msg) -> Maybe String -> Element msg
view instance statusContent contentView maybeExtra404 =
    case statusContent of
        Loading info ->
            loadingView instance info

        Failed error ->
            case error of
                Http.BadStatus 404 ->
                    case maybeExtra404 of
                        Just extra404 ->
                            infoView extra404

                        Nothing ->
                            failedView instance error

                _ ->
                    failedView instance error

        Loaded data ->
            contentView data
