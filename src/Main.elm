module Main exposing (main)

import Branch exposing (BranchName(..))
import Browser exposing (Document)
import Browser.Navigation as Navigation
import Element exposing (Element)
import Html
import Page
import Page.E404 as E404
import Page.ProjectDetail as ProjectDetail
import Page.ProjectList as ProjectList
import Project.Messages
import Route exposing (Route)
import Session exposing (Session)
import Url exposing (Url)


type Model
    = Redirect Session
    | ProjectDetail ProjectDetail.Model
    | ProjectList ProjectList.Model


init : Value -> Url.Url -> Navigation.Key -> ( Model, Cmd Msg )
init _ url key =
    changeRouteTo
        (Route.fromUrl url)
        (Redirect (Session.usernameToSession Nothing key))



-- View


view : Model -> Document Msg
view model =
    let
        viewProject : (String -> Element msg -> Document msg) -> String -> Element msg -> (msg -> Msg) -> Document Msg
        viewProject wrapper pageTitle page toMsg =
            let
                { title, body } =
                    wrapper pageTitle page
            in
            { title = title
            , body = List.map (Html.map toMsg) body
            }
    in
    case model of
        ProjectDetail projectDetailModel ->
            viewProject Page.projectView
                "Project"
                (ProjectDetail.view projectDetailModel)
                GotProjectDetailMsg

        ProjectList projectListModel ->
            Page.fullscreenView "Projects" (ProjectList.view projectListModel)

        _ ->
            Page.fullscreenView "404" (E404.view "Page not found")



-- Update


type alias Value =
    {}


type Msg
    = UrlRequested Browser.UrlRequest
    | UrlChanged Url
    | GotProjectListMsg ProjectList.Msg
    | GotProjectDetailMsg Project.Messages.Msg


toSession : Model -> Session
toSession model =
    case model of
        Redirect session ->
            session

        ProjectDetail projectDetailModel ->
            projectDetailModel.session

        ProjectList projectListModel ->
            projectListModel.session


changeRouteTo : Maybe Route -> Model -> ( Model, Cmd Msg )
changeRouteTo maybeRoute model =
    let
        session =
            toSession model
    in
    case maybeRoute of
        Nothing ->
            ( model, Cmd.none )

        Just (Route.ProjectDetail pid) ->
            ProjectDetail.init session pid (BranchName "master")
                |> updateWith ProjectDetail GotProjectDetailMsg

        Just Route.ProjectList ->
            ProjectList.init session
                |> updateWith ProjectList GotProjectListMsg

        _ ->
            ( model, Cmd.none )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case ( msg, model ) of
        ( UrlRequested urlRequested, _ ) ->
            case urlRequested of
                Browser.Internal url ->
                    case url.fragment of
                        Just _ ->
                            ( model
                            , Navigation.pushUrl
                                (Session.navigationKey (toSession model))
                                (Url.toString url)
                            )

                        Nothing ->
                            ( model, Cmd.none )

                Browser.External href ->
                    ( model
                    , Navigation.load href
                    )

        ( UrlChanged urlChanged, _ ) ->
            changeRouteTo (Route.fromUrl urlChanged) model

        ( GotProjectDetailMsg subMsg, ProjectDetail projectDetailModel ) ->
            ProjectDetail.update subMsg projectDetailModel
                |> updateWith ProjectDetail GotProjectDetailMsg

        ( GotProjectListMsg subMsg, ProjectList projectListModel ) ->
            ProjectList.update subMsg projectListModel
                |> updateWith ProjectList GotProjectListMsg

        ( _, _ ) ->
            -- Ignore messages for the wrong page.
            ( model, Cmd.none )


{-| Creates a new Main Model and Msg from a Page's Model and Msg.
I.e. Main.Model.ProjectList vom Page.ProjectList.{Model,Msg}
-}
updateWith : (subModel -> Model) -> (subMsg -> Msg) -> ( subModel, Cmd subMsg ) -> ( Model, Cmd Msg )
updateWith toModel toMsg ( subModel, subCmd ) =
    ( toModel subModel
    , Cmd.map toMsg subCmd
    )



-- Subscriptions


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none



-- Main


main : Program Value Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlRequest = UrlRequested
        , onUrlChange = UrlChanged
        }
