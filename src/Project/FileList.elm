module Project.FileList exposing (fetch, view)

import Api
import Branch exposing (BranchName)
import Element
    exposing
        ( Attribute
        , Element
        , centerX
        , column
        , el
        , fill
        , mouseOver
        , padding
        , pointer
        , px
        , row
        , spacing
        , text
        , width
        )
import Element.Background as Background
import Element.Events as Events
import Element.Font as Font
import FeatherIcons
import Json.Decode as Decode
import Page.Status as Status exposing (Status)
import Page.Utils as Utils
import Project exposing (FileType, Inode, PID)
import Project.Messages exposing (Msg(..))
import Style



-- API


fetch : PID -> BranchName -> List String -> Cmd Msg
fetch pid branchName path =
    Api.get
        (Api.projectFilesEndpoint pid branchName path)
        GotProjectFiles
        (Decode.list Project.fileDecoder)



-- View


fileIcon : Inode -> Element msg
fileIcon file =
    let
        getIcon fileType =
            case fileType of
                Project.File ->
                    FeatherIcons.fileText

                Project.Directory ->
                    FeatherIcons.folder

                Project.Link ->
                    FeatherIcons.link

                Project.GitModule ->
                    FeatherIcons.gitPullRequest

                Project.Unknown ->
                    FeatherIcons.helpCircle
    in
    getIcon file.fileType |> Utils.listIcon


printPath : List String -> Element msg
printPath dirs =
    let
        elem : String -> Bool -> Element msg
        elem dir active =
            el
                (Utils.ifAppend (not active) (Font.color Style.lightBlue) [])
                (text dir)

        slash : Bool -> Element msg
        slash active =
            el
                (Utils.ifAppend
                    (not active)
                    (Font.color Style.lightBlue)
                    [ width (px 16) ]
                )
                (el [ centerX ] (text "/"))

        buildElementList : List String -> List (Element msg)
        buildElementList dirList =
            case dirList of
                [] ->
                    [ slash True ]

                x :: [] ->
                    [ slash False, elem x True ]

                x :: xs ->
                    [ slash False, elem x False ] ++ buildElementList xs
    in
    row
        [ Background.color Style.blue
        , Font.color Style.white
        , padding Style.defaultSpace
        , spacing Style.halfSpace
        , width fill
        ]
        (buildElementList dirs)


view : Status (List Inode) -> List String -> Element Msg
view statusInodes path =
    let
        addPointer : FileType -> List (Attribute msg)
        addPointer fileType =
            case fileType of
                Project.Directory ->
                    [ pointer ]

                _ ->
                    []

        inodeBack : List Inode
        inodeBack =
            case path of
                [] ->
                    []

                _ ->
                    [ { fileType = Project.Directory
                      , name = ".."
                      , path = Utils.removeLast path
                      , mode = "040000"
                      }
                    ]

        printFile : Inode -> Element Msg
        printFile inode =
            row
                (addPointer inode.fileType
                    ++ [ width fill
                       , padding Style.defaultSpace
                       , spacing Style.defaultSpace
                       , mouseOver
                            [ Background.color Style.lightBlue
                            , Font.color Style.white
                            ]
                       , Events.onClick (GotClickedFile inode)
                       ]
                )
                [ el [] (fileIcon inode), el [] (text inode.name) ]

        printFileList : List Inode -> Element Msg
        printFileList inodes =
            column Style.projectContent
                (printPath path :: List.map printFile (inodeBack ++ inodes))
    in
    Status.view "files" statusInodes printFileList (Just "No files in project")
