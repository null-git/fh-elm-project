module Project exposing
    ( FileType(..)
    , Inode
    , PID(..)
    , Project
    , decoder
    , fileDecoder
    , pidStr
    )

import Branch exposing (BranchName(..))
import Iso8601
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline as Pipeline
import Time
import User exposing (User)


type PID
    = PID String


type alias Project =
    { id : PID
    , path : String
    , name : String
    , owner : User
    , description : String
    , defaultBranch : BranchName
    , lastActivity : Time.Posix
    , starCount : Int
    , forksCount : Int
    , openIssuesCount : Int
    , commitCount : Int
    }


type FileType
    = File
    | Directory
    | Link
    | GitModule
    | Unknown


type alias Inode =
    { fileType : FileType
    , name : String
    , path : List String
    , mode : String
    }


pidStr : PID -> String
pidStr (PID pid_) =
    pid_



-- Deocder


decodePID : Decoder PID
decodePID =
    Decode.map (String.fromInt >> PID) Decode.int


decoder : Decoder Project
decoder =
    Decode.succeed Project
        |> Pipeline.required "id" decodePID
        |> Pipeline.required "path_with_namespace" Decode.string
        |> Pipeline.required "name" Decode.string
        |> Pipeline.required "owner" User.decoder
        |> Pipeline.optional "description" Decode.string ""
        |> Pipeline.optional "default_branch" Branch.branchNameDecoder (BranchName "master")
        |> Pipeline.required "last_activity_at" Iso8601.decoder
        |> Pipeline.required "star_count" Decode.int
        |> Pipeline.required "forks_count" Decode.int
        |> Pipeline.required "open_issues_count" Decode.int
        |> Pipeline.requiredAt [ "statistics", "commit_count" ] Decode.int


fileDecoder : Decoder Inode
fileDecoder =
    let
        translateFileType : String -> FileType
        translateFileType oldType =
            case oldType of
                "blob" ->
                    File

                "tree" ->
                    Directory

                "commit" ->
                    GitModule

                _ ->
                    Unknown

        toDecoder : String -> String -> String -> String -> Decoder Inode
        toDecoder fileType name path mode =
            Decode.succeed
                (Inode
                    (translateFileType fileType)
                    name
                    (String.split "/" path)
                    mode
                )
    in
    Decode.succeed toDecoder
        |> Pipeline.required "type" Decode.string
        |> Pipeline.required "name" Decode.string
        |> Pipeline.required "path" Decode.string
        |> Pipeline.required "mode" Decode.string
        |> Pipeline.resolve
