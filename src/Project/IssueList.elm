module Project.IssueList exposing (fetch, view)

import Api
import Element
    exposing
        ( Attribute
        , Element
        , alignLeft
        , alignRight
        , column
        , el
        , fill
        , mouseOver
        , padding
        , pointer
        , spacing
        , text
        , width
        , wrappedRow
        )
import Element.Background as Background
import Element.Events as Events
import Element.Font as Font
import FeatherIcons
import Issue exposing (Issue)
import Json.Decode as Decode
import Page.Status as Status exposing (Status)
import Page.Utils as Utils exposing (title)
import Project exposing (PID)
import Project.Messages exposing (Msg(..), Page(..))
import Style


fetch : PID -> Cmd Msg
fetch pid =
    Api.get
        (Api.issueListEndpoint pid)
        GotIssueList
        (Decode.list Issue.decoder)


issueAttrs : Issue -> List (Attribute Msg) -> List (Attribute Msg)
issueAttrs issue attrs =
    attrs
        ++ [ pointer
           , width fill
           , padding Style.defaultSpace
           , spacing Style.defaultSpace
           , mouseOver
                [ Background.color Style.lightBlue
                , Font.color Style.white
                ]
           , Events.onClick (Load (IssueDiscussion issue))
           ]


printIssue : Issue -> Element Msg
printIssue issue =
    if Issue.isOpen issue then
        wrappedRow
            (issueAttrs issue [])
            [ el [ alignLeft, title "Open" ] (Utils.listIcon FeatherIcons.alertCircle)
            , text (Issue.toString issue)
            , el [ Element.alignRight, title "Opened at" ] (text (Utils.dateTime issue.createdAt))
            ]

    else
        wrappedRow
            (issueAttrs issue [ Font.color Style.background ])
            [ el [ alignLeft, title "Resolved" ] (Utils.listIcon FeatherIcons.checkCircle)
            , el [ alignLeft ] (text (Issue.toString issue))
            , el [ alignRight, title "Resolved at" ] (text (Utils.dateTime issue.createdAt))
            ]


sortIssues : List Issue -> List Issue
sortIssues issues =
    let
        stateToInt state =
            case state of
                Issue.Open ->
                    0

                Issue.Closed ->
                    1

        compareState : Issue -> Issue -> Order
        compareState issueA issueB =
            compare (stateToInt issueA.state) (stateToInt issueB.state)
    in
    List.sortWith compareState issues


view : Status (List Issue) -> Element Msg
view statusIssues =
    let
        printIssueList : List Issue -> Element Msg
        printIssueList issues =
            if issues == [] then
                Status.infoView "No issues"

            else
                column Style.projectContent (List.map printIssue (sortIssues issues))
    in
    Status.view "issues" statusIssues printIssueList Nothing
