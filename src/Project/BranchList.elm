module Project.BranchList exposing (fetch, view)

import Api
import Branch exposing (Branch)
import Element
    exposing
        ( Element
        , alignRight
        , column
        , el
        , fill
        , mouseOver
        , none
        , padding
        , pointer
        , row
        , spacing
        , text
        , width
        )
import Element.Background as Background
import Element.Events as Events
import Element.Font as Font
import FeatherIcons
import Json.Decode as Decode
import Page.Status as Status exposing (Status(..))
import Page.Utils as Utils
import Project exposing (PID)
import Project.Messages exposing (Msg(..), Page(..))
import Style
import Time



-- API


fetch : PID -> Cmd Msg
fetch pid =
    Api.get
        (Api.branchListEndpoint pid)
        GotBranchList
        (Decode.list Branch.decoder)



-- View


view : Time.Posix -> Status (List Branch) -> Element Msg
view timeNow statusBranches =
    let
        printBranch : Branch -> Element Msg
        printBranch branch =
            row
                [ pointer
                , width fill
                , padding Style.defaultSpace
                , spacing Style.defaultSpace
                , mouseOver
                    [ Background.color Style.lightBlue
                    , Font.color Style.white
                    ]
                , Events.onClick (Load (ProjectDetail (Just branch.name)))
                ]
                [ Utils.eitherOr branch.merged
                    (el [ Utils.title "Merged" ] (Utils.listIcon FeatherIcons.check))
                    (el [] (Utils.listIcon FeatherIcons.gitBranch))
                , el []
                    (text
                        (Utils.eitherOr branch.default
                            (Branch.toString branch.name ++ " (default)")
                            (Branch.toString branch.name)
                        )
                    )
                , Utils.eitherOr branch.protected
                    (el
                        [ Utils.title "Protected" ]
                        (Utils.listIcon FeatherIcons.lock)
                    )
                    none
                , el
                    [ alignRight
                    , Utils.title ("Last Commit at " ++ Utils.dateTime branch.lastCommit.createdAt)
                    , Style.fontSmall
                    ]
                    (text (Utils.dynamicTime branch.lastCommit.createdAt timeNow))
                ]

        sortedBranches : List Branch -> List Branch
        sortedBranches branches =
            List.reverse (List.sortWith (Utils.compareTime2 .lastCommit .createdAt) branches)

        printBranchList : List Branch -> Element Msg
        printBranchList branches =
            if branches == [] then
                Status.infoView "This seems to be an empty repository"

            else
                column Style.projectContent (List.map printBranch (sortedBranches branches))
    in
    Status.view "branches" statusBranches printBranchList Nothing
