module Branch exposing (Branch, BranchName(..), branchNameDecoder, decoder, fromMaybe, toMaybe, toString)

import Commit exposing (Commit)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline as Pipeline



-- Type


type BranchName
    = BranchName String
    | NoBranch


type alias Branch =
    { name : BranchName
    , default : Bool
    , merged : Bool
    , protected : Bool
    , canPush : Bool
    , lastCommit : Commit
    }



-- Branch helper


toString : BranchName -> String
toString branch =
    case branch of
        BranchName branchName ->
            branchName

        NoBranch ->
            ""


fromMaybe : Maybe String -> BranchName
fromMaybe maybeBranch =
    case maybeBranch of
        Just branch ->
            BranchName branch

        Nothing ->
            NoBranch


toMaybe : BranchName -> Maybe String
toMaybe branch =
    case branch of
        BranchName branchName ->
            Just branchName

        NoBranch ->
            Nothing



-- Decoder


branchNameDecoder : Decoder BranchName
branchNameDecoder =
    Decode.map BranchName Decode.string


decoder : Decoder Branch
decoder =
    Decode.succeed Branch
        |> Pipeline.required "name" branchNameDecoder
        |> Pipeline.required "default" Decode.bool
        |> Pipeline.required "merged" Decode.bool
        |> Pipeline.required "protected" Decode.bool
        |> Pipeline.required "can_push" Decode.bool
        |> Pipeline.required "commit" Commit.decoder
